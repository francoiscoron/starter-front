'use strict';

// Chargement des dépendances
var gulp     = require('gulp'),
$            = require('gulp-load-plugins')(),
browserSync  = require('browser-sync'),
del          = require('del'),
autoprefixer = require('autoprefixer'),
cssnano      = require('cssnano'),
pngquant     = require('imagemin-pngquant');

var reload = browserSync.reload;
var stream = browserSync.stream;

// Variable qui sert à savoir si le flag --prod a été passé pour virer les sourcemaps et minifier les scripts et styles
// Passe aussi le NODE_ENV sur prod pour les libs qui l'utilisent
var production = $.util.env.prod ? $.util.env.prod : false;
production ? process.env.NODE_ENV = 'production' : process.env.NODE_ENV = 'development';

var src = './src/';
var dist = './dist/';

var path = {

	src : {
		css:    src + 'scss/',
		fonts:  src + 'fonts/',
		images: './images/',
		icons:  src + 'icons/',
		js:     src + 'js/',
	},

	dist : {
		css:    dist + 'css/',
		fonts:  dist + 'fonts/',
		images: './images/',
		icons:  dist + 'icons/',
		js:     dist + 'js/',
	},

	refresh : [
		'**/*.html',
		'**/*.php',
	]
}

var config = {
	// browserSync
	proxy: false,
	port: 3000,
	// postCSS
	processors: [
		autoprefixer({
			browsers: [
				'ie >= 9',
				'ie_mob >= 10',
				'ff >= 30',
				'chrome >= 34',
				'safari >= 7',
				'opera >= 23',
				'ios >= 7',
				'android >= 4.4',
				'bb >= 10'
			]
		})
	].concat( production ? cssnano({
		zindex: false,
	}) : [] )
}

/**
* CLEAN
* Supprime le dossier dist
*/

gulp.task('clean', function() {
	return del( [dist] );
});


/**
* STYLE
* Compile les fichiers .scss dans le fichier main.min.css
*/

gulp.task('style', function() {
	return gulp.src( path.src.css + '*.scss' )
		.pipe( $.plumber() )
		.pipe( !production ? $.sourcemaps.init() : $.util.noop() )
		.pipe( $.sass().on( 'error', $.sass.logError ) )
		.pipe( $.postcss(config.processors) )
		.pipe( !production ? $.sourcemaps.write() : $.util.noop() )
		.pipe( $.rename({
			suffix: '.min'
		}))
		.pipe( gulp.dest( path.dist.css ) )
		.pipe( stream() );
});

/**
* IMAGES
* Optimisation des images dans le dossier ./images/
*/

gulp.task('images', function() {
	return gulp.src( path.src.images + '**/*' )
		.pipe( $.plumber() )
		.pipe( !production ? $.imagemin({
			progressive: true,
			verbose: true,
			use: [ pngquant() ]
		}) : $.util.noop())
	.pipe( gulp.dest( path.dist.images ) );
});

/**
 * SPRITE
 * Créé une sprite avec les .svg préfixés ico-
*/

gulp.task('sprite', function(){
	return gulp.src( path.src.icons + '*.svg' )
	.pipe($.plumber())
	.pipe($.svgmin())
	.pipe($.svgSprite({
		shape: {
			dimension: {
				// Set maximum dimensions
				maxWidth: 24,
				maxHeight: 24,
			},
			dest: 'intermediate-svg',
		},
		mode: {
			symbol : {
				dest: './',
				sprite: 'sprite.svg'
			}
		}
	}))
	.pipe( gulp.dest( path.dist.icons ) );
});

/**
* FONTS
* Déplace les fonts dans le dist
*/

gulp.task('fonts', function () {
	return gulp.src( path.src.fonts + '**/*' )
	.pipe( gulp.dest( path.dist.fonts ) );
});

/**
* SCRIPT
* Compile et minifie les scripts. Résolve les includes.
*/

gulp.task('script', function() {
	return gulp.src( path.src.js + '**/*.js' )
		.pipe( $.plumber() )
		.pipe( $.include() )
		.on('error', console.log)
		.pipe( !production ? $.sourcemaps.init() : $.util.noop() )
		.pipe( production ? $.uglify() : $.util.noop() )
		.pipe( !production ? $.sourcemaps.write('maps/', {
			addComment: true,
		}) : $.util.noop() )
		.pipe( gulp.dest( path.dist.js ) )
});

/**
* BUILD
*/

gulp.task('build', ['style', 'fonts', 'script', 'images', 'sprite'], function() {
	$.util.log(`
-------------------------------------
Build lancé en mode: ${$.util.colors.yellow.bold(production ? 'production' : 'développement')}
-------------------------------------`);
});

/**
* WATCH
* Ecoute les changements sur les fichiers src (.scss / .js / .svg) et build les assets
*/

gulp.task('watch', ['build'], function() {
	gulp.watch( path.src.css    + '**/*.scss', ['style'] );
	gulp.watch( path.src.js     + '**/*.js', ['script'] );
	gulp.watch( path.src.icons  + '**/*.svg', ['sprite'] );
});

/**
* SERVE
* Lance browsersync avec la config passée dans l'objet config
*/

gulp.task('serve', ['watch'], function() {
	browserSync.init({
		server: !config.proxy ? {
			baseDir:  './'
		} : false,
		proxy: config.proxy,
		port: config.port
	});
	gulp.watch('**/*.html').on('change', browserSync.reload);
});

/**
 * DEFAULT
 * Par défaut, lance un watch
 */

gulp.task('default', ['watch']);